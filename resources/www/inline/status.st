
status(nodeid, cpuload, publicKey, storageSize, maxStorageSize, uptime, otherNodes, localServices)::=<<
node id: $nodeid$<br>
cpu load: $cpuload$<br>
local storage: <meter value="$storageSize$" min="0" max="$maxStorageSize$"></meter> $storageSize$ of $maxStorageSize$ used<br>
uptime: $uptime$<br>
<br>
node public key: $publicKey$<br>

<h3>Known Nodes In Network</h3>
<ul>
$otherNodes :{ node | <li>$node$</li> }$
</ul>

<h3>Local Running Services</h3>
<table width="100%">
<tr><th>Name</th><th width="15%">Version</th><th width="20%">Swagger</th></tr>
$localServices :{ service | <tr><td><b style="color: #0A0">$service.name$</b></td><td>$service.version$</td><td><a href="$service.swagger$">API Listing</a> <a href="/swagger-ui/index.html?url=$service.swagger$">Swagger-UI</a></td></tr> }$
</table>

>>