package i5.las2peer.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import i5.las2peer.communication.Message;
import i5.las2peer.communication.ServiceDiscoveryContent;
import i5.las2peer.p2p.PastryNodeImpl;
import i5.las2peer.p2p.ServiceNameVersion;
import i5.las2peer.persistency.MalformedXMLException;
import i5.las2peer.testing.MockAgentFactory;
import i5.las2peer.testing.TestSuite;
import i5.las2peer.tools.CryptoException;

public class ServiceAgentTest {

	private static final String servicename = "i5.las2peer.somePackage.AService";
	private static final String serviceversion = "1.0";
	private static final String passphrase = "a passphrase";

	@Test
	public void testCreation() throws CryptoException, L2pSecurityException {
		ServiceAgent testee = ServiceAgent
				.createServiceAgent(ServiceNameVersion.fromString(servicename + "@" + serviceversion), passphrase);

		assertEquals(servicename + "@" + serviceversion, testee.getServiceNameVersion().toString());

		assertTrue(testee.isLocked());

		try {
			testee.unlockPrivateKey("dummy");
			fail("L2pSecurityException expected!");
		} catch (L2pSecurityException e) {
			// intended
			assertTrue(testee.isLocked());
		}

		testee.unlockPrivateKey(passphrase);

		assertFalse(testee.isLocked());
	}

	@Test
	public void testXmlAndBack() throws CryptoException, L2pSecurityException, MalformedXMLException {
		ServiceAgent testee = ServiceAgent
				.createServiceAgent(ServiceNameVersion.fromString(servicename + "@" + serviceversion), passphrase);

		String xml = testee.toXmlString();

		ServiceAgent andBack = ServiceAgent.createFromXml(xml);

		andBack.unlockPrivateKey(passphrase);
	}

	@Test
	public void testServiceDiscovery() {
		try {
			ArrayList<PastryNodeImpl> nodes = TestSuite.launchNetwork(6);

			ServiceAgent testServiceAgent0 = ServiceAgent
					.createServiceAgent(ServiceNameVersion.fromString("i5.las2peer.api.TestService@1.0"), "a pass");
			testServiceAgent0.unlockPrivateKey("a pass");
			nodes.get(0).storeAgent(testServiceAgent0);
			nodes.get(0).registerReceiver(testServiceAgent0);

			ServiceAgent testServiceAgent1 = ServiceAgent
					.createServiceAgent(ServiceNameVersion.fromString("i5.las2peer.api.TestService@1.1"), "a pass");
			testServiceAgent1.unlockPrivateKey("a pass");
			nodes.get(1).storeAgent(testServiceAgent1);
			nodes.get(1).registerReceiver(testServiceAgent1);

			ServiceAgent testServiceAgent2 = ServiceAgent
					.createServiceAgent(ServiceNameVersion.fromString("i5.las2peer.api.TestService@2.0"), "a pass");
			testServiceAgent2.unlockPrivateKey("a pass");
			nodes.get(2).storeAgent(testServiceAgent2);
			nodes.get(2).registerReceiver(testServiceAgent2);

			ServiceAgent testServiceAgent3 = ServiceAgent
					.createServiceAgent(ServiceNameVersion.fromString("i5.las2peer.api.TestService2@1.0"), "a pass");
			testServiceAgent3.unlockPrivateKey("a pass");
			nodes.get(3).storeAgent(testServiceAgent3);
			nodes.get(3).registerReceiver(testServiceAgent3);

			PassphraseAgent userAgent = MockAgentFactory.getAdam();
			userAgent.unlockPrivateKey("adamspass");
			nodes.get(4).storeAgent(userAgent);
			nodes.get(4).registerReceiver(userAgent);

			// invoke (fits)
			Message request = new Message(userAgent, ServiceAgent.serviceNameToTopicId("i5.las2peer.api.TestService"),
					new ServiceDiscoveryContent(ServiceNameVersion.fromString("i5.las2peer.api.TestService@1"), false),
					30000);
			request.setSendingNodeId(nodes.get(4).getNodeId());

			Message[] answers = nodes.get(4).sendMessageAndCollectAnswers(request, 4);

			assertEquals(answers.length, 2);

			boolean found10 = false, found11 = false;
			for (Message m : answers) {
				m.open(userAgent, nodes.get(4));
				ServiceDiscoveryContent c = (ServiceDiscoveryContent) m.getContent();
				if (c.getService().getVersion().toString().equals("1.0")) {
					found10 = true;
				} else if (c.getService().getVersion().toString().equals("1.1")) {
					found11 = true;
				}
			}
			assertTrue(found10);
			assertTrue(found11);

			// invoke (exact)

			request = new Message(userAgent, ServiceAgent.serviceNameToTopicId("i5.las2peer.api.TestService"),
					new ServiceDiscoveryContent(ServiceNameVersion.fromString("i5.las2peer.api.TestService@1.1"), true),
					30000);
			request.setSendingNodeId(nodes.get(4).getNodeId());

			answers = nodes.get(4).sendMessageAndCollectAnswers(request, 4);

			assertEquals(answers.length, 1);

			answers[0].open(userAgent, nodes.get(4));
			ServiceDiscoveryContent c = (ServiceDiscoveryContent) answers[0].getContent();
			assertTrue(c.getService().getVersion().toString().equals("1.1"));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}

}
