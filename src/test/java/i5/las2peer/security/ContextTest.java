package i5.las2peer.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.Assert;
import org.junit.Test;

import i5.las2peer.p2p.Node;
import i5.las2peer.persistency.Envelope;
import i5.las2peer.testing.MockAgentFactory;
import i5.las2peer.testing.TestSuite;

public class ContextTest {

	// @Test
	public void testCreation() {
		try {
			// idea has changes, context agent may be locked!
			Node node = TestSuite.launchNetwork(1).get(0);
			UserAgent eve = MockAgentFactory.getEve();
			try {
				new AgentContext(node, eve);
				Assert.fail("L2pSecurityException expected");
			} catch (L2pSecurityException e) {
			}

			eve.unlockPrivateKey("evespass");
			AgentContext testee = new AgentContext(node, eve);

			assertSame(eve, testee.getMainAgent());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}

	@Test
	public void testRequestGroupAgent() {
		try {
			Node node = TestSuite.launchNetwork(1).get(0);

			UserAgent adam = MockAgentFactory.getAdam();
			adam.unlockPrivateKey("adamspass");

			GroupAgent group1 = MockAgentFactory.getGroup1();
			group1.unlockPrivateKey(adam);
			GroupAgent groupA = MockAgentFactory.getGroupA();
			groupA.unlockPrivateKey(adam);
			GroupAgent groupSuper = GroupAgent.createGroupAgent(new Agent[] { group1, groupA });
			groupSuper.unlockPrivateKey(group1);
			node.storeAgent(group1);
			node.storeAgent(groupA);
			node.storeAgent(groupSuper);

			UserAgent eve = MockAgentFactory.getEve();
			eve.unlockPrivateKey("evespass");

			AgentContext context = new AgentContext(node, eve);

			try {
				GroupAgent a = context.requestGroupAgent(group1.getSafeId());
				assertEquals(a.getSafeId(), group1.getSafeId());
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail("exception thrown: " + e);
			}

			try {
				context.requestGroupAgent(groupA.getSafeId());
				Assert.fail("exception expected");
			} catch (Exception e) {
			}

			try {
				GroupAgent a = context.requestGroupAgent(groupSuper.getSafeId());
				assertEquals(a.getSafeId(), groupSuper.getSafeId());
			} catch (Exception e) {
				e.printStackTrace();
				Assert.fail("exception thrown: " + e);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}

	@Test
	public void testOpenEnvelope() {
		try {
			Node node = TestSuite.launchNetwork(1).get(0);

			UserAgent adam = MockAgentFactory.getAdam();
			adam.unlockPrivateKey("adamspass");

			GroupAgent group1 = MockAgentFactory.getGroup1();
			group1.unlockPrivateKey(adam);
			GroupAgent groupA = MockAgentFactory.getGroupA();
			groupA.unlockPrivateKey(adam);
			GroupAgent groupSuper = GroupAgent.createGroupAgent(new Agent[] { group1, groupA });
			groupSuper.unlockPrivateKey(group1);
			GroupAgent groupSuper2 = GroupAgent.createGroupAgent(new Agent[] { group1, groupA });
			groupSuper2.unlockPrivateKey(group1);
			node.storeAgent(group1);
			node.storeAgent(groupA);
			node.storeAgent(groupSuper);
			node.storeAgent(groupSuper2);

			UserAgent eve = MockAgentFactory.getEve();
			eve.unlockPrivateKey("evespass");

			groupA.unlockPrivateKey(adam);
			groupSuper.unlockPrivateKey(groupA);
			groupSuper2.unlockPrivateKey(groupA);

			AgentContext context = new AgentContext(node, eve);

			Envelope envelope1 = context.createEnvelope("id", "content", groupSuper, groupSuper2);
			Envelope envelopeA = context.createEnvelope("id", "content", groupA);
			context.storeEnvelope(envelope1, groupSuper2);

			try {
				envelopeA.getContent(context.getMainAgent());
				Assert.fail("exception expected");
			} catch (Exception e) {
			}

			node.shutDown();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}
}
