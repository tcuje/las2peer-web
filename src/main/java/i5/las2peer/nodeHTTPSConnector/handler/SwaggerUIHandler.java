package i5.las2peer.nodeHTTPSConnector.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import com.sun.net.httpserver.HttpExchange;

import i5.las2peer.nodeHTTPSConnector.NodeHTTPSConnector;
import i5.las2peer.tools.SimpleTools;

public class SwaggerUIHandler extends AbstractHandler {

	public static final String SWAGGER_UI_PATH = "/swagger-ui";
	public static final String SWAGGER_UI_JAR_PREFIX = "META-INF/resources/webjars/swagger-ui/2.2.10";

	public SwaggerUIHandler(NodeHTTPSConnector connector) {
		super(connector);
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		try {
			// serve files from swagger-ui jar
			String path = exchange.getRequestURI().getPath();
			logger.info("Handler: " + getClass().getCanonicalName() + " Method: " + exchange.getRequestMethod()
					+ " Request-Path: " + path);
			String subPath = path.substring(SWAGGER_UI_PATH.length());
			String query = exchange.getRequestURI().getQuery();
			if (subPath.equalsIgnoreCase("/index.html") && (query == null || query.isEmpty())) {
				sendRedirect(exchange, "index.html?url=/service/", false);
				return;
			}
			String res = SWAGGER_UI_JAR_PREFIX + subPath;
			InputStream is = getClass().getClassLoader().getResourceAsStream(res);
			if (is == null) {
				logger.info("404 (not found)");
				sendStringResponse(exchange, HttpURLConnection.HTTP_NOT_FOUND, "text/plain", "404 (not found)");
				return;
			} else {
				byte[] bytes = SimpleTools.toByteArray(is);
				exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, bytes.length);
				OutputStream os = exchange.getResponseBody();
				os.write(bytes);
				os.close();
				return;
			}
		} catch (Exception e) {
			sendInternalErrorResponse(exchange, "Unknown connector error", e);
		}
	}

}
