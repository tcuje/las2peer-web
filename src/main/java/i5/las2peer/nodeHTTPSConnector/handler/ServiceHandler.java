package i5.las2peer.nodeHTTPSConnector.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import i5.las2peer.nodeHTTPSConnector.NodeHTTPSConnector;
import i5.las2peer.p2p.AgentNotKnownException;
import i5.las2peer.p2p.Node;
import i5.las2peer.p2p.PastryNodeImpl;
import i5.las2peer.p2p.ServiceNameVersion;
import i5.las2peer.p2p.ServiceVersion;
import i5.las2peer.restMapper.RESTResponse;
import i5.las2peer.security.Agent;
import i5.las2peer.security.ServiceAgent;
import i5.las2peer.tools.SimpleTools;
import io.swagger.models.Swagger;
import io.swagger.util.Json;

public class ServiceHandler extends AbstractHandler {

	public static final String SERVICE_PATH = "/service";

	public ServiceHandler(NodeHTTPSConnector connector) {
		super(connector);
	}

	@Override
	public void handle(HttpExchange exchange) {
		try {
			final PastryNodeImpl node = connector.getNode();
			exchange.getResponseHeaders().set("Server-Name", "las2peer " + getClass().getSimpleName());
			final String path = exchange.getRequestURI().getPath();
			logger.info("Handler: " + getClass().getCanonicalName() + " Method: " + exchange.getRequestMethod()
					+ " Request-Path: " + path);
			final String subPathString = path.substring(SERVICE_PATH.length());
			final Path subPath = Paths.get(path.substring(SERVICE_PATH.length()));
			final int pathCount = subPath.getNameCount();
			if (subPathString.isEmpty() || pathCount < 1) {
				sendPlainResponse(exchange, "Please specify a service name");
				return;
			}
			// resolve service name and search for local running versions
			String serviceName = subPath.getName(0).toString();
			String versions = "Service '" + serviceName + "' not running locally";
			List<ServiceVersion> versionList = node.getNodeServiceCache().getLocalServiceVersions(serviceName);
			if (versionList != null && !versionList.isEmpty()) {
				versions = SimpleTools.join(versionList, "\n");
			}
			if (pathCount == 1 || versionList == null || versionList.isEmpty()) {
				// TODO add option to return JSON formatted
				sendPlainResponse(exchange, "Following versions running locally:\n" + versions);
				return;
			}
			// validate service version
			String version = subPath.getName(1).toString();
			ServiceNameVersion snv = new ServiceNameVersion(serviceName, version);
			if (!versionList.contains(snv.getVersion())) {
				sendPlainResponse(exchange, "Service name and version combination unknown");
				return;
			}
			String serviceStateString = "NOT running locally";
			ServiceAgent localServiceAgent = null;
			try {
				localServiceAgent = node.getNodeServiceCache().getLocalService(snv);
				serviceStateString = "is running locally";
			} catch (AgentNotKnownException e) {
			}
			if (pathCount == 2 || localServiceAgent == null) {
				// TODO call service default URL or auto-start the service?
				sendPlainResponse(exchange,
						"Service " + serviceStateString + "\nPlease specify a service method to invoke");
				return;
			}
			// FIXME get actual calling agent
			Agent callingAgent = node.getAnonymous();
			if (pathCount == 3 && subPath.getName(2).toString().equalsIgnoreCase("swagger.json")) {
				String basePath = path.substring(0, path.length() - "swagger.json".length());
				sendSwaggerListing(exchange, node, callingAgent, localServiceAgent, basePath);
				return;
			} else {
				// FIXME create jersey context and call service handle method
				handleServiceInvocation(exchange, node, callingAgent, localServiceAgent);
			}
		} catch (Exception e) {
			sendInternalErrorResponse(exchange, "Unknown connector error", e);
		}
	}

	private void sendSwaggerListing(HttpExchange exchange, Node node, Agent callingAgent,
			ServiceAgent localServiceAgent, String basePath) throws Exception {
		// FIXME remove anonymous from request
		Serializable swagResult = node.invokeLocally(callingAgent, localServiceAgent, "getSwagger",
				new Serializable[0]);
		if (swagResult == null) {
			sendInternalErrorResponse(exchange, "Method invocation 'getSwagger' returned null");
			return;
		}
		if (!(swagResult instanceof String)) {
			sendInternalErrorResponse(exchange,
					"Expected type String got '" + swagResult.getClass().getCanonicalName() + "' instead");
			return;
		}
		// deserialize Swagger
		Swagger swagger;
		try {
			swagger = Json.mapper().reader(Swagger.class).readValue((String) swagResult);
		} catch (Exception e) {
			sendInternalErrorResponse(exchange, "Swagger API declaration not available!", e);
			return;
		}
		swagger.setBasePath(basePath);
		// serialize Swagger API listing into a JSON String
		try {
			String swaggerJson = Json.mapper().writeValueAsString(swagger);
			sendStringResponse(exchange, HttpURLConnection.HTTP_OK, "application/json", swaggerJson);
		} catch (JsonProcessingException e) {
			sendInternalErrorResponse(exchange, "Swagger documentation could not be serialized to JSON", e);
			return;
		}
	}

	private void handleServiceInvocation(HttpExchange exchange, Node node, Agent callingAgent,
			ServiceAgent localServiceAgent) throws Exception {
		final URI requestUri = exchange.getRequestURI();
		Path requestPath = Paths.get(requestUri.getPath());
		URI baseUri = new URI(
				"/" + requestPath.getName(0) + "/" + requestPath.getName(1) + "/" + requestPath.getName(2) + "/");
		// FIXME read request body
		byte[] requestBody = new byte[0];
		HashMap<String, List<String>> headers = new HashMap<>();
		for (Entry<String, List<String>> headerEntry : exchange.getRequestHeaders().entrySet()) {
			headers.put(headerEntry.getKey(), headerEntry.getValue());
		}
		// invoke
		Serializable[] params = new Serializable[] { baseUri, requestUri, exchange.getRequestMethod(), requestBody,
				headers };
		Serializable result = null;
		try {
			result = node.invokeLocally(callingAgent, localServiceAgent, "handle", params);
		} catch (Exception e) {
			sendInternalErrorResponse(exchange, "Service method invocation failed", e);
			return;
		}
		if (result == null) {
			sendInternalErrorResponse(exchange, "Service method invocation returned null response");
		} else if (result instanceof RESTResponse) {
			RESTResponse response = (RESTResponse) result;
			exchange.getResponseHeaders().putAll(response.getHeaders());
			try {
				sendResponseHeaders(exchange, response.getHttpCode(), getResponseLength(response.getBody().length));
				OutputStream os = exchange.getResponseBody();
				if (response.getBody().length > 0) {
					os.write(response.getBody());
				}
				os.close();
			} catch (IOException e) {
				logger.warning(e.toString());
			}
		} else {
			sendInternalErrorResponse(exchange, "Expected " + RESTResponse.class.getCanonicalName() + ", but got "
					+ result.getClass().getCanonicalName() + " instead");
		}
	}

	private void sendResponseHeaders(HttpExchange exchange, int responseCode, long contentLength) throws IOException {
		Headers responseHeaders = exchange.getResponseHeaders();
		// remove CORS headers set by service
		responseHeaders.remove("Access-Control-Allow-Origin");
		responseHeaders.remove("Access-Control-Max-Age");
		responseHeaders.remove("Access-Control-Allow-Headers");
		responseHeaders.remove("Access-Control-Allow-Methods");
		// add connector CORS headers
		// TODO make CORS configurable
		responseHeaders.add("Access-Control-Allow-Origin", "*");
		responseHeaders.add("Access-Control-Max-Age", "60");
		// just reply all requested headers
		String requestedHeaders = exchange.getRequestHeaders().getFirst("Access-Control-Request-Headers");
		if (requestedHeaders != null) {
			if (!requestedHeaders.toLowerCase().contains("authorization")) {
				if (!requestedHeaders.trim().equals("")) {
					requestedHeaders += ", ";
				}
				requestedHeaders += "Authorization";
			}
			responseHeaders.add("Access-Control-Allow-Headers", requestedHeaders);
		}
		responseHeaders.add("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
		exchange.sendResponseHeaders(responseCode, contentLength);
	}

	private long getResponseLength(final long contentLength) {
		if (contentLength == 0) {
			return -1;
		}
		if (contentLength < 0) {
			return 0;
		}
		return contentLength;
	}

}
